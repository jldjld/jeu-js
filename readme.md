## Présentation du contexte

Durant mon cursus de formation de développeur web et mobile à Simplon Lyon, il m'a été demandé de réaliser un jeu en Javascript sans library


# PAN PAN

**<u>IDÉE:</u>** 

  J’ai fait le choix d’utiliser un personnage, que je dessine sur mon temps libre, pour imaginer un jeu de reflexe.
  Le scénario est le suivant, le joueur doit faire déplacer le personnage principal pour qu'il attrape les éléments qui tombent.
  Le joueur a 3 possibilités:

- Attraper les éléments.

- Laisser tomber les éléments

- Donner un coup aux éléments.


## Les Fonctionnalités générales du jeu

- Le personnage principal se déplace sur l'axe horizontal de l'écran au mouvement de la souris.

- Obtenir des points, pour augmenter son score.

- attraper les bons ingrédients et éviter les mauvais ingrédients

- Le personnage à la possibilité d'utiliser deux modes différents en utilisant les touches a et espace du clavier.

- Selon le mode choisi, les comportements des items par rapport au personnage sont différents.

  
## Détail fonctionnalités du personnage principal

2 modes pour un personnage principal, un mode content, un mode pas content.

En mode pas content, le joueur à le choix de changer de mode en tapant au clavier.

Comportement des items en fonction du mode choisi :

- Si le personnage est en mode content et qu'il attrape un bad item ( GAME OVER).
- Si le personnage est en mode content et qu'il attrape un pancake +100 points.
- Si le personnage est en mode content et qu'il laisse tomber un pancake -50 points.
- Si le personnage est en mode pas content et qu'il attrape un bad item +50 points.
- Si le personnage est en mode pas content et qu'il attrape un pancake, on perd 100 points.
- Quel que soit le mode, si il ne touche pas de bad item, rien ne se passe.

## Use case
![panpanusecase.jpg](/images/panpanusecase.jpg)

##  LES CHALLENGES**

######   - faire tomber des objets en continu ( boucle infini)

######   - Créer deux modes de jeu, un mode par défaut, un mode pas content.

###### - Arrêter la fonction qui fait tomber les objets à la fin du timer.

######  \- Créer une interaction quand un objet entre en collision avec la poêle en 2 positions :

 \- pôele pour sauter les pancakes

 \- pôele pour taper les mauvais ingrédients


## ORGANISATION

Un délai de 3 semaines, nous a été donné afin de réaliser ce jeu.
J'ai déterminé mon organisation de la manière suivante, en réalisant un Trello.

<u>SEMAINE 1:</u>

- Définir les fonctionnalités du jeu
- Graphisme du personnage principal, items du jeu, dans les deux mode.
- Développement de la partie front, page d'accueil et page du jeu.

<u>SEMAINE 2:</u>

- Développement d'une fonction pour faire tomber les good items

- Déceloppement d'une fonction pour faire tomber les bad items

- Déterminer des positions aléatoires pour tous les items

- Détection des collisions selon l'item attrapé en mode par défaut

- A l'enfoncement de la touche espace, changer l'image du personnage pour le mode Angry. Avec la touche A, revenir en mode par défaut.

  

<u>SEMAINE 3:</u>

- Récupération/ Affichage du score en fonction des items attrapés par le personnage
- Développement du mode Angry du personnage, en passant par une valeur boléenne. Si le mode Angry est activé le comportement avec les items du jeu change.
- Mise en place d'une durée de jeu, un timer.
- A la fin de ce timer les items doivent arrêter de tomber, le score total doit être affiché.

## MAQUETTE

![maquette](images/maquette.jpg)

<u>SCREENSHOTS//</u>

Ecran d'accueil
![écran accueil](images/écran accueil.png)


Écran de jeu en mode 1 ( mode par défaut)
![Capture d’écran - 2019-09-09 à 15.28.03](images/Capture d’écran - 2019-09-09 à 15.28.03.png)


Écran de jeu en mode 2 ( mode Angry)
![Capture d’écran - 2019-09-09 à 15.30.39](images/Capture d’écran - 2019-09-09 à 15.30.39.png)



<u>Exemple de fonctionnalités développées</u>

Créer des éléments en javascript pour les afficher dasn le DOM

![Capture du 2019-09-12 14-06-08](images/create.png) 

extrait de fonction pour faire tomber les éléments, lorsque ceux ci arrivent à la position 450, et qu'il y a collision entre un élément 
et le personnage principal, alors l'élément est supprimé, et le score du joueur est augmenté.

![Capture du 2019-09-12 14-13-58](images/falling.png)

Les deux modes du personnage principal

![Capture du 2019-09-12 14-07-27](images/change mode.png)

<u>**A mettre en place en v2**</u>

- Arrêter toutes les intéractions à la fin du timer du jeu
- Régler le fait que lorsque qu'un bad item passe derrière le personnage heurte sa hitbox.
- Enregistrer le meilleur score.
- Faire en sorte que les objets tombent de plus en plus vite.


