// VIE

let vies = document.querySelector(".vies");
//SCORE
let score = document.querySelector(".score");
let scoreValue = 0;
// Valeur qui fait passer le mode du personnage à Angry
let angryMode = false;
/* score save test
let scoreHigh = document.querySelector(".bestscore");
scoreHigh  = 100;
clearInterval(timer);*/
// PERSONNAGE PRINCIPAL
let nomnom = document.querySelector("#nomnom"); // faire un nomnom content
nomnom.src = "images/nomnom.png";
//overlay RIP
let divRip = document.createElement('div');






/* LEFT AND RIGHT MOVEMENTS */
document.onmousemove = function (event) {
  let x = event.clientX * 90 / window.innerWidth + "%";
  for (let i = 0; i < 2; i++) {
    nomnom.style.left = x;
  }
}

// CREATE BAD ITEMS
function createBadItems() {
  let img = document.createElement("img");
  img.src = "images/baditem-01.png";
  img.style.width = "20px";
  img.style.height = "65px";
  img.classList.add("badItems");
  let target = document.querySelector("#target");
  target.appendChild(img);
  img.style.left = Math.random() * 950 + 50 + 'px';
  fallingItems(img);
}


// CREATE PANCAKES
function createPancakes() {
  let img = document.createElement("img");
  img.src = "images/pancake2.png";
  img.style.width = "50px";
  img.style.height = "40px";
  img.classList.add("pancakes");
  let target = document.querySelector("#target");
  target.appendChild(img);
  img.style.left = Math.random() * 610 + 120 + 'px';
  fallingItems(img);
}
createPancakes();
createBadItems();
//FAIT TOMBER LES PANCAKES À L'INFINI
dropTimer = setInterval(createPancakes, 1000);
//FAIT TOMBER LES BAD ITEMS À L'INFINI
dropBadTimer = setInterval(createBadItems, 1000);

// TIMER DU JEU
let timer = new Timer();


/* FALLING ELEMENTS*/
function fallingItems(pancake) {
  let pos = 0;
  let id = setInterval(frame, 2);
  function frame() {
    //Démarre le timer du jeu

    //COLLISION DETECTION NOMNOM/
    if (pos >= 450) {
      if ((Math.abs(pancake.y - nomnom.y) <= 60) //HITBOX 
        &&
        (Math.abs(pancake.x - nomnom.x) <= 115) //HITBOX
      ) {
        //résultat collision pancake/nomnom
        if (pancake.classList == "pancakes") {
          if (angryMode == false) {
            scoreValue = parseInt(scoreValue) + 100;
            score.innerHTML = scoreValue;
            
            //pancake.classList.add("vies");
            pancake.remove();
            clearInterval(id);
          } else if (angryMode == true) {
            pancake.remove();
            scoreValue = parseInt(scoreValue) - 50;
            score.innerHTML = scoreValue;
            clearInterval(id);
          }
        }//résultat collision badItem/nomnom
        if (pancake.classList == "badItems") {
          if (angryMode == true) {
            pancake.remove();
            package.style.width = "300px"; 
            package.style.height = "150px"; 
            pancake.classList.add("");
            //ajouter animation explosion ou/et changer de classlist le pancake
            clearInterval(id);
            scoreValue = parseInt(scoreValue) + 50;
            score.innerHTML = scoreValue;
          }
          else if (angryMode == false) {
            vies.remove();
            clearInterval(timer);
            clearInterval(dropTimer);
            clearInterval(dropBadTimer);
            divRip.classList.add('gameover');
            document.body.appendChild(divRip);
            divRip.textContent = "R.I.P nomnom is pancaketarian! click to start a new life...";
            /*let logo = document.createElement('img');
            logo.classList.add("ripnomnom");
            divWin.appendChild(logo);*/
            
            divRip.addEventListener('click', function () {
              location.reload();
            })
          }
        }
        
      }
    }

    //HIT GROUND/
    if (pos == 580) {
      if (pancake.classList == "pancakes") {
        pancake.remove();
        //ces deux lignes à faire si class est pancake
        scoreValue--;
        score.innerHTML = scoreValue;
        clearInterval(id);
      } else {
        pancake.remove();
      }
    } else {
      pos++;
      pancake.style.top = pos + 'px';
    }
    
  }
}

//booléen angryMode qui est de base à false
//CHANGE CHARACTER
document.addEventListener('keyup', function (e) {
  if (e.keyCode == 32) {
    angryMode = true;
    nomnom.src = "images/transparentangry.png";
    document.body.style.backgroundColor = "#000000";
  }
})
document.addEventListener('keyup', function (e) {
  if (e.keyCode == 65) {
    angryMode = false;
    nomnom.src = "images/nomnom.png";
    document.body.style.backgroundColor = "#e9cab6";

  }
})
//TIMER
function Timer() {
  let i = 1;
  let timer = setInterval(function () {
    let time = document.querySelector(".timer");
    time.innerHTML = i;
    //console.log(i);
    i++;
    // END TIMER
    if (i > 20) {
      clearInterval(timer);
      clearInterval(dropTimer);
      clearInterval(dropBadTimer);
      divRip.remove();
      pancake.x = null;
      pancake.y = null;
      //PANNEAU GAME OVER 
      let divWin = document.createElement('div');
      divWin.classList.add('overlay');
      document.body.appendChild(divWin);
      divWin.textContent = "You got " + score.innerHTML + " points click to restart !";
      divWin.addEventListener('click', function () {
        location.reload();
      });

    }
  }, 1000);
 
}